# Intro aux "bonnes pratiques" de programmation
## Sommaire:
- [Intro aux "bonnes pratiques" de programmation](#intro-aux-"bonnes-pratiques"-de-programmation)
    - [Bonnes pratiques de programmation](#bonnes-pratiques-de-programmation)
    - [Caractéristiques d'un code bien écrit ?](#caractéristiques-dun-code-bien-écrit)
        - [Le code doit être facile à lire](#le-code-doit-être-facile-à-lire)
        - [Le code doit avoir une organisation logique et évidente](#le-code-doit-avoir-une-organisation-logique-et-évidente)
        - [Le code doit être explicite](#le-code-doit-être-explicite)
        - [Le code doit être soigné et robuste au temps qui passe](#le-code-doit-être-soigné-et-robuste-au-temps-qui-passe)
    - [Bien joli tout ça, mais coder proprement ça prend du temps !](#bien-joli-tout-ça-mais-coder-proprement-ça-prend-du-temps)
    - [Importance des commentaires](#importance-des-commentaires)
    - [Comment nommer les choses ?](#comment-nommer-les-choses)
    - [À propos des environnements de développement](#à-propos-des-environnements-de-développement)
    - [Le travail en équipe avec git](#le-travail-en-équipe-avec-git)
    - [Géré les erreurs](#géré-les-erreurs)
    - [La sécu et le principe de moindre privilège](#la-sécu-et-le-principe-de-moindre-privilège)

##  Bonnes pratiques de programmation

Lorsque l'on programme, on passe un certain temps à écrire du code, mais on passe beaucoup plus de temps à lire du code que soi, ou d'autres, ont écrit.

![goodPractices](./pics/goodPractices.png)

On peut facilement faire l'analogie entre l'écriture du code et l'écriture d'un texte. Lorsque le texte est mal écrit, qu'il contient des fautes d'orthographe, que les phrases sont mal structurées et que les idées ne sont pas organisées, ce texte est très difficile à lire et donc à comprendre. Il en va de même pour le code : un code brouillon est très difficile et fatiguant à comprendre... de plus, les bugs s'y cachent beaucoup plus facilement. 

Par exemple :

```py
def k(i: int) -> int:
    # rxkfghh = rxkfgh2 sauf si i = 1
    rxkfghh="gh"
    if i==1:
        p=7
        a=p+2
        rxkfgh=1
        rxkfghh="temporaire"
        print(rxkfghh, rxkfgh)
    else:
        rxkfghh2="tempoaussi"
        rxkfgh = i*k(i-1)
        print(rxkfghh, rxkfgh)
    return rxkfgh
```
Sans grande surprise... **Ça il ne faut absolument pas le faire ! 🤢**
Tu reconnais ce que fait cet algorithme ? Il calcule des factorielles. Mais ce serait plus simple à comprendre si l'algorithme était bien écrit ! 

Il est donc très important de respecter certaines bonnes pratiques de programmation (qui s'appliquent naturellement à la rédaction d'algorithmes) pour rendre le plus agréable possible la lecture de code.

## Caractéristiques

Un algorithme, ou code "bien écrit" doit avoir les propriétés suivantes :
- Être facile à lire, pas soi-même, mais aussi par les autres.
- Avoir une organisation logique et évidente.
- Être explicite, montrer clairement les intentions du développeur.
- Être soigné et robuste au temps qui passe.

Nous allons regarder un peu plus en détails chacune de ces caractéristiques.

### Le code doit être facile à lire

Pour que le code soit facile à lire, il faut d'une part qu'il soit bien structuré et bien présenté, et d'autre part, que les noms des variables et des fonctions soient choisis avec soin.

Pour ce qui est de la structure et de la présentation, Python nous aide beaucoup, car le langage impose beaucoup de choses qui nous "forcent" à bien présenter notre code. Par exemple, les blocs d'instructions du même niveau doivent être précédés du même nombre d'espaces, ce qui nous conduit naturellement à bien indenter notre code.

Dans d'autres langages, plus de libertés de présentation sont offertes au développeur, par conséquent, il convient de se forcer à respecter des conventions pour écrire le code le plus propre possible.

Par exemple, regarde le code Java ci-dessous. ⬇️ Il est évident que le manque d'indentation ne facilite pas la lecture et la compréhension du code...

```js
int i,j,k, m, n, o = 0;
n = 9; o=13;
int [][] p = new int [n][o];
for(i=0;i<n;i++){
p[i][0] = i;
m = i;
for(j=1;j<o;j++){
for(k=0;k<j-1;k++){m = m + p[i][k];}
p[i][j] = m;}}
System.out.println("----");
for(i=0;i<o;i++){
for(j=0;j<n;j++){
System.out.print("|" + p[j][i]);}
System.out.println("|");}
```

Il devrait plutôt ressembler à ça au niveau de L'indentation: 
```js
int i,j,k, m, n, o = 0;
n = 9; o=13;
int [][] p = new int [n][o];
for(i=0;i<n;i++){
    p[i][0] = i;
    m = i;
    for(j=1;j<o;j++){
        for(k=0;k<j-1;k++){
            m = m + p[i][k];
        }
        p[i][j] = m;
    }
}
System.out.println("----");
for(i=0;i<o;i++){
    for(j=0;j<n;j++){
        System.out.print("|" + p[j][i]);
    }
    System.out.println("|");
}
```
Pour ce qui est du choix des noms des choses (variables et fonctions), on en parle juste après ! 😉

### Le code doit avoir une organisation logique et évidente

Ce point est plus délicat, car nous avons souvent des solutions différentes pour résoudre le même problème. Il est donc normal qu'un code qui semble logique à quelqu'un semble "tordu" à son voisin.

Étant conscient de cela, il faut nous efforcer de trouver des solutions logiques aux problèmes que nous devons résoudre et d'éviter d'emprunter des chemins plus compliqués qui ne feraient que semer la confusion.

Par exemple, si l'on nous demande d'afficher tous les nombres de 1 à 10, il suffit de faire une boucle qui fait évoluer un compteur entre 1 et 10 et qui affiche, à chaque tour, la valeur de ce compteur. La solution qui consisterait à faire une boucle qui fait évoluer un compteur de 9 à 0 et qui afficherait à chaque tour le résultat de 10 - compteur fonctionne aussi, mais est à proscrire, car elle est "tordue".

### Le code doit être explicite

Lorsque l'on écrit des algorithmes ou que l'on développe des programmes, on est parfois tenté de prendre des raccourcis car "on sait" que telle ou telle méthode permet de faire telle ou telle chose bien pratique.

Il n'est pas interdit de prendre ces raccourcis, mais il faut toujours prendre le soin d'expliquer, au moins à travers des commentaires, pourquoi on fait cela. C'est important à la fois pour permettre aux autres de comprendre pourquoi ta solution est astucieuse... mais aussi pour toi, au cas où tu ne te souviens plus de "pourquoi tu as fait ça".

Par exemple, si tu dois afficher une matrice de dimensions MxM, la procédure usuelle est de faire deux boucles imbriquées permettant d'afficher chacun des éléments de la matrice. Or, si on sait que notre matrice est triangulaire, on va probablement vouloir optimiser notre double boucle d'affichage. C'est naturellement une bonne idée... mais il faut penser à rappeler dans le commentaire pourquoi on a procédé de la sorte.

### Le code doit être soigné et robuste au temps qui passe

Lorsque l'on écrit du code, on a la fâcheuse tendance à s'arrêter dès que celui-ci fonctionne. C'est un tort ! Le code doit être entretenu. Cela signifie qu'il faut relire son code après l'avoir terminé, vérifié que l'on a bien supprimé les éléments obsolètes, vérifier que les commentaires sont à jour et cohérents avec le code conservé, etc.

Cette opération de "maintenance" du code est cruciale, mais elle est pourtant souvent négligée par beaucoup, ce qui peut poser des problèmes, notamment lorsque vous rencontrez un bug.

L'exemple le plus classique est celui-ci : tu implémentes une méthode `tri` qui permet de trier les éléments d'un tableau. Tu n'es pas satisfaite du comportement de cette méthode lorsque tu l'utilises depuis ton programme principal. Tu implémentes donc une autre méthode `test` qui utilise une autre stratégie pour trier les éléments du tableau. Cette méthode marche mieux. Tu l'utilises ainsi dans ton programme principal. Ton programme fonctionne et tu passes à autre chose, sans penser à intégrer tes modifications proprement dans ton programme. Quelques jours plus tard, tu reprends ton code et tu observes un bug. Tu penses que cela provient du tri du tableau. Tu vas ainsi observer ce qui se passe dans `tri`. Après quelques heures de recherche, tu es furieuse contre toi-même, car tu réalises enfin que la méthode `tri` n'est plus utilisée depuis longtemps dans ton code... 🤯

Crois-moi, les problèmes de ce type arrivent beaucoup plus souvent qu'on ne le pense... surtout quand on cherche à faire vite.

## Bien joli tout ça, mais coder proprement ça prend du temps

Alors déjà... C'est faux ! 
Il ne faut pas confondre vitesse et précipitation.

On a souvent tendance à penser que l'on perd énormément de temps à soigner son code, à le structurer correctement, à le réorganiser et à le documenter, mais c'est faux. Au contraire, on gagne du temps à faire tout cela.

Voici quelques explications pour t'en convaincre :

Si tu adoptes les bonnes pratiques dès le début, tu fais déjà 50% du travail.
Si le code est bien écrit, il est plus facile, et donc plus rapide à relire, et n'oublie pas que tu passes plus de temps à lire ton code qu'à l'écrire... donc quand ton code est propre, tu **te** fais gagner du temps.
Si le code est logique et bien structuré, il sera plus simple de retrouver les bugs qu'il contient, et ainsi de l'améliorer.
C'est ainsi autant de raisons qui devraient te convaincre qu'il est important d'être organisé, clair, méthodique et rigoureux quand tu développes.

## Importance des commentaires

Les commentaires sont essentiels pour "éclairer" le code. Un commentaire est un texte qui est ignoré par l'ordinateur lorsqu'il exécute le programme, mais qui peut être lu par le développeur lorsqu'il lit le programme.

En Python, une ligne qui débute par le signe `#` est un commentaire. On peut aussi faire des blocs de commentaires, sur plusieurs lignes, en utilisant les triples guillemets.

Bien que les commentaires soient essentiels, il ne faut pas en abuser.

![comments](./pics/comments.png)

Un bon commentaire peut :

- Faciliter la lecture du code,
- Apporter une indication sur un choix de conception,
- Expliquer une motivation qui ne serait pas évidente (comme dans l'exemple de la matrice triangulaire vu plus haut)
- Donner un exemple pour permettre de mieux comprendre ce que fait le code.

Quelques exemples de mauvais commentaires :

- Un commentaire qui décrit un morceau de code qui n'existe plus,
- Un commentaire qui explique une évidence,
- Un commentaire sur plusieurs lignes pour expliquer une chose simple.
- Un commentaire sur l'historique des modifications d'un fichier. C'est parfois utile, mais dans la plupart des cas, il vaut mieux confier cette tâche à votre gestionnaire de versions qui fera le travail pour vous. (Genre Git)

Une petite liste d'exemples de mauvais commentaires :

```py
i = 0 #initialisation de la variable i à 0
i = i + 1 # incrémentation de la variable i
# Additionne a et b et stocke le résultat dans c
c = a + b
# Ci-dessous une double boucle pour afficher un tableau
for i in range(10):
    print("Valeur ", i)
# fin du for
# Et maintenant, on va s'occuper de retourner la valeur de i
# Pour cela, on utilise le mot clé return
# Et on passe ensuite la valeur de i
return i
```
Typiquement ce que je fais dans les premiers exercices de TP1 c'est pas fou.. Mais il faut bien apprendre. 🤗

Comme tu le vois dans l'exemple ci-dessus, il n'est pas judicieux d'utiliser un commentaire pour signaler la fin d'un bloc d'instructions. En général, si tu as besoin de ce type de commentaire, c'est que ton code est déjà trop long. Dans ce cas, demande-toi si tu ne peux pas découper ton code en fragments plus simples.

Attention : les commentaires ne doivent pas pallier un manque de clarté du code. Si tu as besoin de commentaires pour cela, c'est probablement que tu peux améliorer ton code pour le rendre plus lisible. Essaye donc de le refactorer, c'est-à-dire de le ré-écrire, au moins partiellement, en l'améliorant.

Comme on l'a dit plus haut, les commentaires, tout comme le code, doivent être maintenus, c'est-à-dire qu'ils doivent évoluer avec le code, et disparaître si le code disparaît. Par conséquent, il faut veiller au bon dosage de vos commentaires, de sorte à ne pas alourdir inutilement votre travail de maintenance.

## Comment nommer les choses

Les noms que tu choisis pour tes variables et tes fonctions vont grandement contribuer à la lisibilité de votre code.

Par exemple, tu seras ok que le bloc de code suivant n'est pas très lisible tandis que celui d'après, qui fait exactement la même chose, est plus clair.

```py
xretyers = 4
eijfzeipfjzpeij = 1
xretyers = eijfzeipfjzpeij + xretyers
```
```py
xretyers = 4
eijfzeipfjzpeij = 1
xretyers = eijfzeipfjzpeij + xretyers
```

La première règle est donc de choisir des noms de variables prononçables et faciles à retenir.

Tu dois choisir des noms de variables explicites pour toi, mais aussi pour les autres.

Par exemple, `a` est bien moins explicite que `adresseClien`. De même, `lf` est moins explicite que `largeurFenetre`. Pense également que tu seras probablement amenés à chercher tes variables dans ton code avec un outil de recherche. À ton avis, combien d'occurrences de `a` tu trouverais ? Et combien d'occurrences de `adresseClient` ?

Évite également de choisir des noms de variables qui induisent un contre sense. Par exemple, si tu écris `matrice=8`, on pourrait penser que la variable est une matrice, or, il s'agit clairement d'un entier. Au moment de l'affectation, il est facile de se rendre compte du type de la variable, mais maintenant, imagine que tu rencontres, au beau milieu du code, la ligne suivante :
```py
matrice = matrice * 4
```
Comment tu vas interpréter cette instruction ?

Évite également les noms de variable qui n'ont pas de sens, comme `plop`, surtout si tu en utilises plusieurs dans la même portion de code. Tu risques de te perdre dans les noms et donc d'introduire inutilement des bugs dans ton code.

Ne triche pas non plus quand tu choisis vos noms de variables. Dans la plupart des langages de programmation, certains mots sont "réservés" car ce sont des instructions du langage. Par exemple, en Python, on ne peut pas nommer une variable `if` ou `list` car ce sont des mots réservés du langage. Si tu penses avoir besoin de ces mots pour nommer tes variables, ne triche pas en écrivant `iff` ou `llist`, tu risques de te perdre dans tes propres astuces.

La plupart des langages imposent des règles pour le choix des noms de variables. Par exemple, en Python, un nom de variable ne peut pas commencer par un chiffre (il y a d'autres règles). En plus de ces règles exigées par les langages, essaie de ne pas utiliser des caractères qui pourraient porter à confusion. En particulier, les caractères `o 0 O l i et 1` peuvent se ressembler fortement, voire être identiques en fonction de la police de caractères utilisée pour l'affichage à l'écran.

Enfin, essaie d'être cohérente lorsque tu choisis tes noms de variables. Par exemple, si tu décides d'utiliser le français pour nommer tes variables (Et fait pas ça), utilise le français tout du long, n'alterne pas avec l'anglais. `longueurPath` ou `lenghtChemin` serait bien étrange et clairement moche !

## À propos des environnements de développement

Tu as à ta disposition des environnements de développement (abrégé EDI en français ou **IDE** en anglais, **(pour integrated development environment)**).
Dans notre cas on à le magnifique Visual Studio Code !
Pense à l'utiliser et apprend à exploiter ses nombreuses possibilités. (L'IDE s'addapte aux languages utiliser !)
Pour l'expérience essayée de faire un petit code directement dans le terminal et tu comprendras que les IDE c'est sacrément la vie.

La plupart de ces environnements offrent des outils de refactoring qui te permettent de réorganiser ton code facilement tout en garantissant une certaine cohérence. Par exemple, ils te permettent de renommer une variable partout dans le code. C'est l'occasion de changer tous tes `plop` par des noms plus ignificatifs. 🧐

En général, ces outils permettent également de vérifier que votre code est correctement indenté (VScode le fait tout seul 🤗), et qu'il respecte des conventions de programmation préétablies et matérialisées par des règles. Tu dois apprendre à les utiliser, cela te fera gagner beaucoup de temps.

Ces outils t'offrent également des fonctionnalités d'auto-complétion (c'est-à-dire qu'ils complètent automatiquement le texte que vous êtes en train de taper). Et ça... c'est ouf ! Ça permet de ne pas faire d'erreurs de frappe déjà.
C'est des fonctionnalités très pratiques qu'il faut aussi apprendre à maîtriser. Grâce à ces fonctionnalités, vous n'avez plus d'excuses : inutile de choisir des noms de variables courts (type a b c) sous prétexte que c'est plus rapide à taper...
Et encore mieux, pas besoin de se souvenir de tous les noms, car il te les propose !

## Le travail en équipe avec git

Sur de gros projets, il est bien rare de travailler seul. Et c'est bien pour cela que les "bonnes pratiques" sont **extrêmement importantes**. Pour la simple raison que si tout le monde code comme un sauvage et bien pour réunir les différents codes et comprendre ce qu'on fait les autres, ce serait l'enfer. 

### Travailler en equipe comment qu'on fait ?

On s'envoie des morceaux de code par mail à chaque modification. 
Alors non bien heureusement. À la place, on utilise git!
Je t'invite à lir le [petit doc sur git](../../notions/git/README.md). Et c'est cadeau, je t'en fais un résumer express **git,** c'est vraiment le feu ! Et ce même pour bosser seul, car git te permet de stocker, sauvegarder et partager ton travail. Et tout ça en ligne sur un serveur externe. 
Si ton Pc deciissait de rendre l'âme. Dont worry, si ton travail est sur git tu y as accès depuis n’importe quel ordinateur.😌
Donc git ça doit devenir une de tes habitudes. D'autant plus que c'est réellement simple à utiliser.

Donc !
Dans un premier temps lorsque tu commences un travail de programmation. Tu crées un repo git. Une fois cela, fais-tu Clone ce repo git sur ton Pc et une fois à l'intérieur, tu peux enfin commencer à coder sereinement. À chaque avancée, un petit `push` et tu continues.
Git fait du versioning, donc il te permet de pouvoir récupérer une ancienne version de ton code (si par exemple, tu as tout cassé, c'est parfois plus rapide de revenir en arrière 😬).

**"okai, mais du coup pour le travail en équipe ?"** 

On y arrive mademoiselle. 

### Travailler en equipe

En équipe en principe, on se divise le travail. On ne doit jamais bosser à plusieurs sur une même partie de code.(ça semble évident, mais dans les projets, je ne cesse de me battre pour avoir une bonne organisation et qu'on ne passe pas notre temps à faire les mêmes choses)
On travaille chacun sur un morceau different et puis on `git pull` et `git push` pour partager avec les copains. 🤗
Et c'est tout... 
Si on a peur de mettre le bazar chez les autres on peut créer des `branches` différentes pour chaque personne et ne les faire se rejoindre seulement lorsqu'on le souhaite. 

Dans le travail en équipe le plus important est donc l'organisation. Bien répartir les tâches en fonction des talents de chacun est la clé pour produire un résultat satisfaisant en un minimum de temps.
Il faut pour cela définir des `règles de nomages` et construire une `architecture logicielle afin d'avoir une constance dans le projet peu importe qui a codé tel parti.


## Gérer les erreurs 

La gestion des erreurs fait pleinement partie des choses dont tu dois prendre l'habitude. C'est inévitable tes codes auront des bugs. Le tout c'est d'en éviter un maximum et de les repérer rapidement. Et pour cela dans nos conditions lors de la programmation on prend en compte que tout ne se passera pas nécessairement bien. 

par exemple: 
```py
def IsNegative(nb: int) : # definition avec argument (affichant "int" lorsqu'elle est appelée")
    if isinstance(nb, int) : # Vérifie si l'argument est un entier
    suite du code..
```
Ma fonction a pour but de vérifier si l'argument en entrée est inférieur à zéro ou non. Mais si je mettais une lettre en argument ? 🤪
Résultat le programme cesse son exécution. Il nous affiche qu'il y a un problème avec cette fonction alors qu'en réalité, elle fonctionne très bien... juste que l'argument était mauvais. 
Donc, on anticipe!! Et on autorise **seulement** ce qui est nécessaire au fonctionnement de la partie de code que l'ont créé.
⬇️ À ce propos d'ailleurs ! ⬇️

## La sécu et le principe de moindre privilège

Dans les bonnes pratiques, il faut aussi prendre l'habitude d'écrire un algorithme qui fait et ne fera jamais que ce dont tu as besoin qu'il fasse. Cela évite les comportements inattendus et simplifie le code.
Une fonction fait une tache. Et seulement une...
Un dossier contient tel type de fichier et aucun autre. Il faut tout structurer et compartimenter tout le temps.
Une variable a et doit conserver un type fixe, qui ne change pas au cours du temps. (même si c'est possible en python, car les variables sonts des `pointeurs`)
D'autre part dans les projets un peu plus conséquents, les permissions et users qui lisent/écrivent/exécute ton code doivent être bien choisi. car si tout le monde a le droit de modifier ton code alors, on peut aisaiment lancer du code (donc des virus) sur la machine. Cela s'appel le [principe de moindre privilège](../least_privilege_principle/README.md).

[Haut de page](#bonnes-pratiques-de-programmation)

