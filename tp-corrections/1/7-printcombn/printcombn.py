# TP-1 printcombn
# Alexandre Pajak

# Instructions:

# Write a function that prints all possible combinations of n different digits in ascending order.
# n will be defined as : 0 < n < 10
# Below are the references for the printing format expected.
# (for n = 1) '0, 1, 2, 3, ..., 8, 9'
# (for n = 3) '012, 013, 014, 015, 016, 017, 018, 019, 023,...689, 789'

# Expected function:
# def PrintCombN(n: int):

# Usage:
# Here is a possible program to test your function :
# def main() :
#   PrintCombN(1)
#   PrintCombN(3)
#   PrintCombN(9)


# And its output :
# % python3 printcomb.py
# 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
# 012, 013, 014, 015, 016, 017, 018, ... 679, 689, 789
# 012345678, 012345679, ..., 123456789
# %

# Warning !!! -->   Be mindful of your program efficiency to avoid timeouts..


#### code: ####


def PrintCombN(n: int):
    result = ""
    comb = ""
    combList = []

    if isinstance(n, int):  # check if n is a int
        if n > 0 and n < 10:  # check if n is 0 < n <
            power = pow(10, n)  # calculate how many number we need
            for nb in range(0, power):  # loop 10^n times for gnerate all numbers
                percent = int(nb / power * 100)  # percentage of programme run
                print("runing at: {}%".format(percent))
                numberLen = len(str(nb))  # get the number (nb) len
                index = 0  # reset index at 0 for every loop
                unitList = []  # reset unitList for every loop
                for n in range(
                    0, numberLen
                ):  # loop for each number in nb (ex: 123 = 3 loop | 3, 2 and 1)
                    index += 1  # increment index
                    div = int(
                        pow(10, index) / 10
                    )  # adapt div for each number un loop (ex: 123 | for get 3 we need div = 1, for 2 div = 10...)
                    unit = int((nb / div) % 10)  # calcul unit with printcomb calcul
                    unitList.append(unit)  # add result in list
                if len(unitList) == 1:  # if len of number is 1 (ex nb = 8)
                    combList.append(str(unitList[0]))  # add the unit to combList
                else:
                    for index in range(
                        0, len(unitList) - 1
                    ):  # else we parse all unit in list
                        comb = ""
                        if (
                            unitList[index] <= unitList[index + 1]
                        ):  # compare if the first is less or equal to second and second ..
                            break  # if is true we stop the loop. !!!!! unit list is a reversed list of unit (ex: nb=123 list=[3,2,1]
                        elif (
                            index == len(unitList) - 2
                        ):  # else if index is the last is ok all number are checked
                            for (
                                unit
                            ) in (
                                unitList
                            ):  # so combinaition is ok and we add it to a string (comb)
                                comb = str(unit) + comb  # revers add it
                            combList.append(
                                comb
                            )  # we have for nb=123 comb=123 & for nb=321 comb= nothing so add to comblist
                    for index in range(
                        0, len(combList)
                    ):  # loop in this new list of good combination
                        while (
                            len(combList[index]) < len(str(power)) - 1
                        ):  # loop in list for add 0 (n=3 nb=1 --> n=3 nb=001)
                            combList[index] = (
                                "0" + combList[index]
                            )  # (n=4 nb=12 --> nb=0012)

            result = ", ".join(
                combList
            )  # for all in combList less last we concatenat it to result with comma and space
            print(result)

        else:
            print("error, bad argument 'n' must be in 0 < n < 10 !")
    else:
        print("error, bad argument 'n' isn't an integer !")


def main():
    PrintCombN(6)


main()
