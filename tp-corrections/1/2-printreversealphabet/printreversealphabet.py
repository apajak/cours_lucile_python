# TP-1 printReversAlphabet
# Alexandre Pajak

# Instructions:
# Write a program that prints the Latin alphabet in lowercase in reverse order (from 'z' to 'a') on a single line.

# Usage:
# % python3 printalphabet.py 
# zyxwvutsrqponmlkjihgfedcba
# %


#### code: ####

# with while loop function:
def printReversAlphabet():
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    reversAlphabet = "" 
    index = len(alphabet) # len(alphabet) return an integer variable equal to : 26

    while index > 0:
        reversAlphabet += alphabet[index-1]
        print(reversAlphabet)
        index -= 1 # decrement index for add another letter
    print(reversAlphabet)

# run printReversAlphabet()
printReversAlphabet()
