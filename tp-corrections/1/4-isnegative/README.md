# TP1

## Isnegative

### Objectif:
Dans cet exercice nous devons afficher "T" (True) si la variable (nb) passer en paramètre de notre fonction Isnegative(nb) est strictement inférieur à 0. S'il ne l'est pas c'est donc qu'il est supérieur ou égal.


### Methode:
Les paramètres sont des variables placées entre les parenthèses d'une fonction et utiliser à l'intérieur de celle-ci pour produire un certain résultat. Globalement toute fonction possède des paramètres car c'est grâce à cela que l'on peut adapter une fonction à différentes situations et accomplir des tâches.  
Par exemple la fonction print(), déjà présente dans Python, affiche l'argument qu'on lui donne.
exemple:
```py
print("argument") # affiche argument
```
Dans notre cas, le but est de créer une fonction qui nous dira en fonction de l'argument qu'on lui donne s'il est négatif ou pas.

Pour cela nous devons créer des conditions. Elles permettent d'exécuter du code seulement si elles sont remplies. On peut de cette manière reguiller le chemin d'exécution du programme en fonction de ses arguments. 

La première condition sera donc de vérifier si l'argument est bien un int, car dans le cas contraire notre fonction générera une erreur ("à" < 0 ne pouvant pas être résolu) et l'exécution du programme se stoppera. si l'argument n'est pas un int on demande à la fonction d'afficher un message d'erreur et de passer son chemin de manière à ce que le programme continu.

La seconde sera de vérifier si Nb est inférieur à 0. Si oui on affiche "T" autrement "F".

> Les condition if/elif/else permettent d'éguiller l'execution du programme.<br/>
> Il est nécessaire d'inclure des paramètres a une fonction pour la rendre polyvalante.<br/>
> Il est important de commancer a prendre l'habitude de géré les erreurs.<br/>

## code:
[suggestion de code](isnegative.py)
