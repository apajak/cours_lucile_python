# TP-1 printcomb
# Alexandre Pajak

# Instructions:
# Write a function that prints, in ascending order and on a single line: all unique combinations
# of three different digits so that, the first digit is lower than the second, 
# and the second is lower than the third. 
# These combinations are separated by a comma and a space.


# Expected function:
# def PrintComb():

# Usage:
# Here is a possible program to test your function :
# def main() :
#   Printcomb()


# And its output :
# % python3 printcomb.py
# 012, 013, 014, 015, 016, 017, 018, 019, 023, ..., 689, 789$
# %

# Info:
# 000 or 999 are not valid combinations because the digits are not different.
# 987 should not be shown because the first digit is not less than the second.


#### code: ####

def Printcomb() : # fonction without argument beacause she do only one thing
    combList = []
    for nb in range(0,1000) :   # loop who generate a nb numbers between 0 and 1000
        hundreds = int(nb/100) # get number of hundreads of nb
        tens = int((nb/10)%10) # get number of tens of nb
        unit = int(nb%10)      # get unit of nb

        if hundreds < tens < unit : # if condition to verify firt number is less than second and second less than third
            combList.append(str(hundreds)+str(tens)+str(unit)) # if true add it to combList (string convertion)
    
    result = ", ".join(combList) # for all in combList less last we concatenat it to result with comma and space
    print(result)



def main():
    Printcomb()

main()