# TP1

## PrintComb

### Objectif:
Dans cet exercice nous avons besoin d'un peu des mathématiques 😋 pour créer une liste de nombres à trois chiffres toujours avec les centaines inférieures aux dizaines et dizaine inférieure aux unités. En suivant nous devons les afficher proprement séparer par une virgule.


### Methode:
Pour cet exercice un peu plus complexe il est nécessaire de savoir isoler les centaines, dizaines et unités d'un nombre compris entre 0 et 1000 par le calcul. 🥵 <br/>
Pour les centaines: il suffit de faire le nombre (nb) divisé par 100.<br/>
Pour les dizaines: il faut faire nb divisé par 10 pour enlever les centaines puis le reste modulo 10 (noté: %) pour enlever les unités.<br/>
Pour les unités: il faut faire nb % 10.<br/>

> Le modulo est le reste d'une division euclidienne (12 % 10 = 2 car on peut diviser 12 une seule fois par 10 et il reste 2)

Sachant cela une des marches a suivre peut-être de faire une boucle qui crée tous les nombres entre 0 et 1000.

##### Pour chacune des boucles:
- On récupère le nombre généré.
- On fait nos trois calculent pour avoir le nombre de centaines, dizaine, et unité de nb.
- On compare les trois de manière à vérifier notre condition (centaines < dizaine < unités)
- Si la condition est vérifiée on ajoute ce nombre à un tableau temporaire.

Une fois notre tableau rempli avec nos bons nombres il ne nous reste plus qu'à l'afficher proprement.
Pour cela on fait une boucle qui lit chacun élement de notre tableau jusqu'à l'avant-dernier. converti cet element en un string que l'on ajoute à notre variable réponse de type string grace à la concaténation. (sans oublier la virgule et l'espace)

On n'oublie pas le dernier élément qui lui n'a pas besoin de virgule ni d'espace. ☝️

Il ne reste plus qu'à print le résultat ! 🤗

donc:
> - Les maths peuvent être bien utiles pour résoudre des problèmes logiques en programmation.<br/>
> - Il peut être intéressant de créer de variable temporaire qui permet de stocker un résultat sous une forme intermédiaire le temps de le traiter.<br/>
> - Les boucles sont ton meilleur allié pour traiter un grand nombre d'opérations.<br/>
> - Les tableaux où les listes permettent de stocker et manipuler de nombreuses informations. <br/>

#### Bien joué celui-ci n'était pas facile ! 😌
## code:
[suggestion de code](printcomb.py)
