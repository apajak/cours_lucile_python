# Correction du TP1

Toutes mes codes ne sont que des propositions, il y a évidement plusieurs manières de résoudre chaque problème.<br>
Pour les débuts tout sera commenter mais petit a petit tu lira le code sans que les commentaires ne soit utiles alors ils disparaitrons.. <br>
Dans chaque dossier il y a un `README.md` qui tente d'expliquer le raisonement derière la résolution des problèmes.

- [PrintAlphabet](./1-printalphabet)
- [PrintReversAlphabet](./2-printreversealphabet)
- [PrintDigits](./3-printdigits)
- [IsNegative](./4-isnegative)
- [PrintComb](./5-printcomb)
- [PrintComb2](./6-printcomb2)
- [PrintCombn](./7-printcombn)
- [PrintCombn_bis](./7-printcombn_bis)
