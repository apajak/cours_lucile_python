# TP-1 printcombn_bis
# Alexandre Pajak

# Instructions:

# Write a function that prints all possible combinations of n different digits in ascending order.
# n will be defined as : 0 < n < 10
# Below are the references for the printing format expected.
# (for n = 1) '0, 1, 2, 3, ..., 8, 9'
# (for n = 3) '012, 013, 014, 015, 016, 017, 018, 019, 023,...689, 789'

# Expected function:
# def PrintCombN(n: int):

# Usage:
# Here is a possible program to test your function :
# def main() :
#   PrintCombN(1)
#   PrintCombN(3)
#   PrintCombN(9)


# And its output :
# % python3 printcomb.py
# 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
# 012, 013, 014, 015, 016, 017, 018, ... 679, 689, 789
# 012345678, 012345679, ..., 123456789
# %

# Warning !!! -->   Be mindful of your program efficiency to avoid timeouts..


#### code: ####
from numpy import *

combList = []


def lastComb(n: int):
    if n < 10:
        index = -1
        limit = 10
        lastCombTab = []
        numpyLastCombTab = empty(n, int)
        for i in range(0, n):
            numpyLastCombTab[index] = limit + index
            index -= 1
        for num in numpyLastCombTab:
            lastCombTab.append(num)
        return lastCombTab
    else:
        return "error will be <= 9."


def firstComb(n: int) -> list:
    if n < 10:
        return list(range(0, n))
    else:
        return "error will be <= 9."


def printCombList(n: int):
    global combList
    result = ""
    for num in firstComb(n):
        result += str(num)
    result += ", "
    for comb in combList:
        for num in comb:
            result = result + str(num)
        result = result + ", "
    print(result)


# Recursive function for find the n+1 combination of n numbers.
def PrintCombN(comb: list, n: int):
    global combList
    lastCombTab = lastComb(n)
    nextNumberindex = -1
    newComb = comb
    if isinstance(n, int):
        if n > 0 and n < 10:
            if newComb[nextNumberindex] < 9:
                newComb[nextNumberindex] += 1
                print(newComb)
            else:
                for i in range(0, n):
                    if lastCombTab[i] == newComb[i]:
                        newComb[i - 1] += 1
                        for y in range(i, n):
                            newComb[y] = newComb[y - 1] + 1
                        print(newComb)
                        break
            combList.append(list(newComb))
            if lastCombTab != newComb:
                PrintCombN(newComb, n)
            else:
                printCombList(n)
        else:
            print("error, bad argument 'n' must be in 0 < n < 10 !")
    else:
        print("error, bad argument 'n' isn't an integer !")


def main():
    # n must be :  0 < n < 10
    n = 6
    PrintCombN(firstComb(n), n)


main()
