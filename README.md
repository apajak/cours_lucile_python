# Lucile Python

Ici seront déposés tous les supports de cours liés au cours de Python.

### ➜ [**Cours**](./cours/README.md)

- [Introduction aux "bonnes pratiques" de l'informatique](./cours/cours/intro/README.md)
- [Introduction a Python](./cours/cours/intro_python/README.md)

### ➜ [**TP**](./tp/README.md)

- [TP1 : Bases python](./tp/1/README.md)
- [TP2 : List et Récursivité](./tp/2/README.md)

### ➜ [**TP Lucile**](./tp-Lucile/README.md)

- [TP1 : Bases python](./tp-Lucile/1/README.md)
- [TP2 : List et Récursivité](./tp-Lucile/1/README.md)

### ➜ [**corrections TP**](./tp-corrections/README.md)

- [TP1 : Bases python](./tp-corrections/1/README.md)
