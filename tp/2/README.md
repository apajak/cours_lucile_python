# Intro a python (List et Récursivité).

Dans ce second TP on va travailler sur des fonctions plus mathématiques histoire de te permettre d'améliorer ton algorithmie. <Br>
Tu vas aussi pas mal Utiliser la récursivité qui est pour moi vraiment pas facile à intégrer alors que pourtant la récursivité est hyper pratique !

## sommaire

- [BasicAtoi](#basicatoi)
- [Atoi](#atoi)
- [SortIntegerTable](#sortintegertable)
- [IterativeFactorial](#iterativefactorial)
- [RecursiveFactorial](#recursivefactorial)
- [IterativePower](#iterativepower)
- [RecursivePower](#recursivepower)
- [Fibonacci](#fibonacci)
- [Sqrt](#sqrt)
- [Isprime](#isprime)
- [FindnextPrime](#findnextprime)


<br><br>
## BasicAtoi

### Instructions:

Write a function that simulates the behaviour of the Atoi function in Go. <br>
Atoi transforms a number defined as a string in a number defined as an int. <br>

Atoi returns 0 if the string is not considered as a valid number.<br>
For this exercise only valid string will be tested. They will only contain one or several digits as characters.<br>

For this exercise the handling of the signs + or - does not have to be taken into account.<br>
This function will only have to return the int. For this exercise the error return of Atoi is not required.

### Expected function:

```py
def basicAtoi(s str)-> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
   print(basicAtoi("12345"))
   print(basicAtoi("0000000012345"))
   print(basicAtoi("000000"))
```
### And its output :
```py
% python3 basicatoi.py
 12345
 12345
 0
%
```
<br><br>
## Atoi

### Instructions:

Write a function that simulates the behaviour of the Atoi function in Go. <br>
Atoi transforms a number represented as a string in a number represented as an int.<br>

Atoi returns 0 if the string is not considered as a valid number.<br>
For this exercise non-valid string chains will be tested. Some will contain non-digits characters.<br>

For this exercise the handling of the signs + or - does have to be taken into account.<br>
This function will only have to return the int. For this exercise the error result of Atoi is not required.

### Expected function:

```py
def atoi(s str)-> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(atoi("12345"))
    print(atoi("0000000012345"))
    print(atoi("012 345"))
    print(atoi("Hello World!"))
    print(atoi("+1234"))
    print(atoi("-1234"))
    print(atoi("++1234"))
    print(atoi("--1234"))
```
### And its output :
```py
% python3 atoi.py
12345
12345
0
0
1234
-1234
0
0
%
```


<br><br>
## SortIntegerTable

### Instructions:

Write a function that reorders a slice of int in ascending order.

### Expected function:

```py
def sortIntegerTable(s list)-> list:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    integerTable = [5,4,3,2,1,0]
    print(sortIntegerTable(integerTable))

```
### And its output :
```py
% python3 sortintegertable.py
[0, 1, 2, 3, 4, 5]
```


<br><br>
## IterativeFactorial

### Instructions:

Write an iterative function that returns the factorial of the int passed as parameter.<br>
Errors (non possible values or overflows) will return 0.<br>

### Expected function:

```py
def iterativeFactorial(nb: int)-> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(iterativeFactorial(4))

```
### And its output :
```py
% python3 iterativefactorial.py
24
%
```

<br><br>
## RecursiveFactorial

### Instructions:

Write a recursive function that returns the factorial of the int passed as parameter.<br>
Errors (non possible values or overflows) will return 0.<br>
for is forbidden for this exercise.<br>

### Expected function:

```py
def recursiveFactorial(nb: int)-> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(recursiveFactorial(4))

```
### And its output :
```py
% python3 recursivefactorial.py
24
%
```

<br><br>
## IterativePower

### Instructions:

Write an iterative function that returns the value of nb to the power of power.<br>
Negative powers will return 0. Overflows do not have to be dealt with.<br>

### Expected function:

```py
def iterativePower(nb: int, power: int)-> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(iterativePower(4, 3))

```
### And its output :
```py
% python3 iterativepower.py
64
%
```


<br><br>
## RecursivePower

### Instructions:

Write a recursive function that returns the value of nb to the power of power.<br>
Negative powers will return 0. Overflows do not have to be dealt with.<br>
for is forbidden for this exercise.<br>

### Expected function:

```py
def recursivePower(nb: int, power: int)-> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(recursivePower(4, 3))

```
### And its output :
```py
% python3 recursivepower.py
64
%
```

<br><br>
## Fibonacci

### Instructions:

Write a recursive function that returns the value at the position index in the fibonacci sequence.<br>
The first value is at index 0.<br>
The sequence starts this way: 0, 1, 1, 2, 3 etc...<br>
A negative index will return -1.<br>
for is forbidden for this exercise.<br>

### Expected function:

```py
def fibonacci(index int) -> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(fibonacci(4))

```
### And its output :
```py
% python3 fibonacci.py
3
%
```

<br><br>
## sqrt

### Instructions:

Write a function that returns the square root of the int passed as parameter,<br>
if that square root is a whole number. Otherwise it returns 0.<br>

### Expected function:

```py
def sqrt(number: int) -> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(sqrt(4))
    print(sqrt(3))

```
### And its output :
```py
% python3 fibonacci.py
2
0
%
```

<br><br>
## isprime

### Instructions:

Write a function that returns true if the int passed as parameter is a prime number. Otherwise it returns false.<br>
The function must be optimized in order to avoid time-outs with the tester.<br>
(We consider that only positive numbers can be prime numbers)<br>
(We also consider that 1 is not a prime number)<br>

### Expected function:

```py
def isprime(number: int) -> bool:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(isprime(5))
    print(isprime(4))

```
### And its output :
```py
% python3 isprime.py
True
False
%
```
<br><br>
## findnextprime

### Instructions:

Write a function that returns the first prime number that is equal or superior to the int passed as parameter. <br>
The function must be optimized in order to avoid time-outs with the tester.<br>
(We consider that only positive numbers can be prime numbers)<br>

### Expected function:

```py
def findNextPrime(number: int) -> int:
```


### Usage:

Here is a possible program to test your function :
```py
def main() :
    print(findNextPrime(5))
    print(findNextPrime(4))

```
### And its output :
```py
% python3 findnextprime.py
5
5
%
```
